#!/usr/bin/env python3
"""AmonEcole example
"""


from json import dumps, loads
from http.server import HTTPServer, SimpleHTTPRequestHandler
from tiramisu_json import TiramisuJson
from examples.AmonEcole import amonecole


class RequestHandler(SimpleHTTPRequestHandler):
    """main
    """
    def __init__(self,
                 *args,
                 **kwargs) -> None:
        self.config = amonecole.get_config()
        self.config.property.read_write()
        self.config.property.add('demoting_error_warning')
        # self.config.property.add('expert')
        # self.config.property.add('normal')
        self.config.permissive.add('expert')
        self.config.permissive.add('normal')
        self.tiramisu_web = TiramisuJson(self.config,
                                         clearable='all',
                                         remotable='minimum',
                                         root='creole')
        super().__init__(*args, **kwargs)

    def set_header(self):
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()

    def do_GET(self):
        """Get variables data"""
        print('GET variables')
        self.set_header()
        form = [{'type': 'submit', 'title': 'Configurer'}]
        # Add collapse for tiramisu-web-ui
        if 'normal' not in self.config.property.get():
            for descr in self.config.unrestraint.option('creole').list(type='optiondescription'):
                form.append({'key': descr.option.path(),
                             'collapse': True})
        values = self.tiramisu_web.get_jsonform(form)
        self.wfile.write(dumps(values).encode())

    def do_POST(self):
        """Post data"""
        self.set_header()
        body = loads(self.rfile.read(int(self.headers.get('content-length', 0))))
        print("POST {} on {}".format(body['updates'], self.path))
        values = self.tiramisu_web.set_updates(body)
        values['message'] = {'text': 'OK', 'type': 'info'}
        self.wfile.write(dumps(values).encode())


def main(port=8000):
    """Launches the http server
    """
    print('Listening on http://localhost:%s' % port)
    server = HTTPServer(('', port), RequestHandler)
    server.serve_forever()


if __name__ == "__main__":
    main()
