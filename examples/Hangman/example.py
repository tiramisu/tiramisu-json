#!/usr/bin/env python3
"""Hangman example
"""


from http.server import HTTPServer, SimpleHTTPRequestHandler
from random import choice
import unicodedata
import re
from json import dumps, loads
from tiramisu import RegexpOption, OptionDescription, Config, IntOption, \
                     UnicodeOption, BoolOption, ParamOption, Params
from tiramisu_json import TiramisuJson


LANG = 'fr_FR'
DICT_FILE = '/usr/share/myspell/{}.dic'.format(LANG)
WORD_REGEXP = re.compile(r'^[a-z]{7,12}$')
PROPOSALS_LEN = 27
NB_PROPOSALS = 6


def remove_accent(word):
    """remove all accent"""
    word = unicodedata.normalize('NFD', word)
    return word.encode('ascii', 'ignore').decode()


def get_random_word():
    """get line randomly in myspell file
    """
    with open(DICT_FILE, 'r') as file_content:
        word = choice(file_content.readlines()).strip()
        if word.endswith('/S.'):
            word = word[:-3]
        if word.endswith('/X.'):
            word = word[:-3]
        if word.endswith('/F.'):
            word = word[:-3]
        if word.endswith('/a0p+') or word.endswith('/d0p+') or word.endswith('/a3p+'):
            word = word[:-5]
        if not WORD_REGEXP.search(remove_accent(word)):
            return get_random_word()
        return word


def display_uncomplete_word(word, *proposals):
    """display response with proposals
    """
    if display_proposals_left(display_misses(word, *proposals)) == 0:
        return word
    display = ['-'] * len(word)
    for idx, char in enumerate(remove_accent(word)):
        if char in proposals:
            display[idx] = word[idx]
    return ''.join(display)


def display_misses(word, *proposals):
    """display all proposals
    """
    ret = list(set(proposals) - set(list(remove_accent(word))))
    if None in ret:
        ret.remove(None)
    ret.sort()
    return ' '.join(ret)


def validate_misses(misses):
    if display_proposals_left(misses) == 0:
        raise ValueError('No more guest possible')


def display_proposals_left(misses):
    if not misses:
        return NB_PROPOSALS
    return max(NB_PROPOSALS - len(misses.split(' ')), 0)


def display_proposal(word, *proposals):
    if display_uncomplete_word(word, *proposals) == word:
        return False
    return display_proposals_left(display_misses(word, *proposals)) != 0


class ProposalOption(RegexpOption):
    __slots__ = tuple()
    _regexp = re.compile(r'^[a-z]$')
    _display_name = 'proposal'
#
#
#class HangmanUI:
#    footer_text = [
#        ('title', "Example Data Browser"), "    ",
#        ('key', "UP"), ",", ('key', "DOWN"), ",",
#        ('key', "PAGE UP"), ",", ('key', "PAGE DOWN"),
#        "  ",
#        ('key', "+"), ",",
#        ('key', "-"), "  ",
#        ('key', "LEFT"), "  ",
#        ('key', "HOME"), "  ",
#        ('key', "END"), "  ",
#        ('key', "F8"), " quit"
#        ]
#
#    def __init__(self,
#                 config):
#        self.listbox = TiramisuTui(config)
#        self.listbox.offset_rows = 1
#        self.header = urwid.Text('Hangman')
#        self.footer = urwid.AttrWrap(urwid.Text(self.footer_text),
#                                     'foot')
#        self.view = urwid.Frame(
#            urwid.AttrWrap(self.listbox, 'body'),
#            header=urwid.AttrWrap(self.header, 'head'),
#            footer=self.footer)
#
#        self.listbox.set_frame(self.view)
#        # Run the program
#        self.loop = urwid.MainLoop(self.view,
#                                   get_palette(),
#                                   unhandled_input=self.unhandled_input)
#        self.loop.run()
#
#    def unhandled_input(self, k):
#        if k in ('f8',):
#            raise urwid.ExitMainLoop()
#

class Hangman():
    def __init__(self):
        options = []
        proposal = None
        word = UnicodeOption('word',
                             'Word',
                             properties=('hidden', 'force_store_value'),
                             callback=get_random_word)
        proposals = [ParamOption(word)]
        for idx in range(PROPOSALS_LEN):
            requires = [{'option': 'self',
                         'expected': None,
                         'action': 'hidden',
                         'inverse': True}]
            if proposal is not None:
                display = BoolOption('display{}'.format(idx),
                                     'Display {}'.format(idx),
                                     properties=('hidden',),
                                     callback=display_proposal,
                                     callback_params=Params(tuple(proposals)))
                options.append(display)
                requires.append({'option': proposal,
                                 'expected': None,
                                 'action': 'disabled'})
                requires.append({'option': display,
                                 'expected': False,
                                 'action': 'disabled'})

            proposal = ProposalOption('guess{}'.format(idx),
                                      'Guess {}'.format(idx),
                                      requires=requires,
                                      properties=('positional',))

            proposals.append(ParamOption(proposal, True))
            options.append(proposal)
        #
        proposal_word = UnicodeOption('proposal_word',
                                      'Word',
                                      properties=('frozen',),
                                      callback=display_uncomplete_word,
                                      callback_params=Params(tuple(proposals)))
        misses = UnicodeOption('misses',
                               'Misses',
                               properties=('frozen',),
                               callback=display_misses,
                               callback_params=Params(tuple(proposals)),
                               validator=validate_misses)
        proposals_left = IntOption('proposals_left',
                                   'Proposals left',
                                   properties=('frozen',),
                                   callback=display_proposals_left,
                                   callback_params=Params(ParamOption(misses)))
        config = Config(OptionDescription('root',
                                          'root',
                                          options + [word, proposal_word, misses, proposals_left]))
        config.property.read_write()
        config.property.add('demoting_error_warning')
        # self.config.property.add('expert')
        # self.config.property.add('normal')
        config.permissive.add('expert')
        config.permissive.add('normal')
        self.tiramisu_web = TiramisuJson(config,
                                         clearable='all',
                                         remotable='minimum')


class RequestHandler(SimpleHTTPRequestHandler):
    """main
    """
    def __init__(self,
                 *args,
                 **kwargs) -> None:
        self.tiramisu_web = hangman.tiramisu_web
        super().__init__(*args, **kwargs)

    def set_header(self):
        self.send_response(200)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()

    def do_GET(self):
        """Get variables data"""
        print('GET variables')
        self.set_header()
        form = [{'type': 'submit', 'title': 'Configurer'}]
        values = self.tiramisu_web.get_jsonform(form)
        from pprint import pprint
        #pprint(values)
        print(values['model'][len(values['model']) - 4])
        self.wfile.write(dumps(values).encode())

    def do_POST(self):
        """Post data"""
        self.set_header()
        body = loads(self.rfile.read(int(self.headers.get('content-length', 0))))
        print("POST {} on {}".format(body['updates'], self.path))
        values = self.tiramisu_web.set_updates(body)
        values['message'] = {'text': 'OK', 'type': 'info'}
        # print('====', values)
        self.wfile.write(dumps(values).encode())


def main(port=8000):
    """Launches the http server
    """
    print('Listening on http://localhost:%s' % port)
    global hangman
    hangman = Hangman()
    print(')))))))))))))))))))))))))))))))')
    server = HTTPServer(('', port), RequestHandler)
    server.serve_forever()


if __name__ == "__main__":
    main()
