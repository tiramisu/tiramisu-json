#!/usr/bin/env python3


from tiramisu import Config, OptionDescription
from tiramisu.error import PropertiesOptionError
from tiramisu_json import TiramisuJson


from os import listdir, makedirs
from os.path import join, splitext, isdir
from importlib import import_module
from json import dump, load
from shutil import rmtree


def list_data(ext='.py'):
    datadir = join('test', 'data')
    filenames = listdir(datadir)
    filenames.sort()
    for filename in filenames:
        if filename.endswith(ext) and not filename.startswith('__'):
            yield datadir.replace('/', '.') + '.' + splitext(filename)[0]


def error_to_str(dico):
    new_dico = {}
    for key, value in dico.items():
        if isinstance(value, list):
            for idx, val in enumerate(value):
                if isinstance(val, Exception):
                    value[idx] = str(val)
        elif isinstance(value, Exception):
            value = str(value)
        new_dico[key] = value
    return new_dico


def write_json(modname, descr, root, prefix):
    suffix = filename.rsplit('.', 1)[1]
    #
    config = Config(descr)
    config.property.add('demoting_error_warning')
    if 'get_values' in dir(mod):
        mod.get_values(config, root is not None)
    config.property.read_write()
    #
    tiramisu = TiramisuJson(config,
                            remotable='minimum',
                            clearable='minimum',
                            root=root)
    form = [{'title': 'Configurer',
             'type': 'submit'}]
    values = tiramisu.get_jsonform(form)
    json_name = 'test_tiramisu_api_json/{}{}.json'.format(prefix, suffix)
    with open(json_name, 'w') as fh:
        dump(values, fh)
    #
    json_name = 'test_tiramisu_api_json/{}{}.dict'.format(prefix, suffix)
    dico = error_to_str(config.value.dict())
    with open(json_name, 'w') as fh:
        dump(dico, fh)
    #
    data = {}
    for key in dico:
        data[key] = {}
        if config.option(key).option.isfollower():
            indexes = list(range(config.option(key).value.len()))
        else:
            indexes = [None]
        for index in indexes:
            try:
                data[key][index] = config.option(key, index).owner.get()
            except PropertiesOptionError:
                pass
    json_name = 'test_tiramisu_api_json/{}{}.owner'.format(prefix, suffix)
    with open(json_name, 'w') as fh:
        dump(data, fh)
    #
    data = {}
    for key in dico:
        data[key] = {}
        if config.option(key).option.isfollower():
            indexes = list(range(config.option(key).value.len()))
        else:
            indexes = [None]
        for index in indexes:
            try:
                data[key][index] = list(config.option(key, index).property.get())
            except PropertiesOptionError:
                pass
    json_name = 'test_tiramisu_api_json/{}{}.prop'.format(prefix, suffix)
    with open(json_name, 'w') as fh:
        dump(data, fh)
    #
    data = {}
    for key in dico:
        data[key] = {}
        if config.option(key).option.isfollower():
            indexes = list(range(config.option(key).value.len()))
        else:
            indexes = [None]
        for index in indexes:
            try:
                data[key][index] = list(config.option(key, index).property.get(True))
            except PropertiesOptionError:
                pass
    json_name = 'test_tiramisu_api_json/{}{}.prop2'.format(prefix, suffix)
    with open(json_name, 'w') as fh:
        dump(data, fh)
    #
    data = {}
    for key in dico:
        data[key] = {}
        data[key] = {}
        for func in ['doc', 'name', 'isoptiondescription',
                     'isleader', 'isfollower', 'issymlinkoption',
                     'ismulti', 'type']:
            value = getattr(config.option(key).option, func)()
            if isinstance(value, frozenset):
                value = list(value)
            data[key][func] = value
    json_name = 'test_tiramisu_api_json/{}{}.info'.format(prefix, suffix)
    with open(json_name, 'w') as fh:
        dump(data, fh)


if isdir('test_tiramisu_api_json'):
    rmtree('test_tiramisu_api_json')
makedirs('test_tiramisu_api_json')
for filename in list_data():
    modname = filename.rsplit('.', 1)[1]
    mod = import_module(filename)
    #
    descr = mod.get_description()
    write_json(modname, descr, None, '')
    #
    descr = mod.get_description()
    descr = OptionDescription('root', '', [descr])
    write_json(modname, descr, modname, 'subconfig_')


i = 0
while True:
    i += 1
    lists = list(list_data('.mod{}'.format(i)))
    if not lists:
        break
    for filename in lists:
        json_name = filename.replace('.', '/') + '.mod{}'.format(i)
        with open(json_name, 'r') as fh:
            dico = load(fh)
        json_name = 'test_tiramisu_api_json/{}.mod{}'.format(filename.split('.')[-1], i)
        with open(json_name, 'w') as fh:
            dump(dico, fh)
        #
        json_name = filename.replace('.', '/') + '.updates{}'.format(i)
        with open(json_name, 'r') as fh:
            dico = load(fh)
        json_name = 'test_tiramisu_api_json/{}.updates{}'.format(filename.split('.')[-1], i)
        with open(json_name, 'w') as fh:
            dump(dico, fh)
        #
        json_name = filename.replace('.', '/') + '.dict{}'.format(i)
        with open(json_name, 'r') as fh:
            dico = load(fh)
        json_name = 'test_tiramisu_api_json/{}.dict{}'.format(filename.split('.')[-1], i)
        with open(json_name, 'w') as fh:
            dump(dico, fh)
