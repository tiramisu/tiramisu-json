from tiramisu.option import ChoiceOption, StrOption, OptionDescription

def get_description():
    """generate description for this test
    """
    option1 = ChoiceOption('choice', "Choice description", ("hide", "show"), default='hide', properties=('mandatory',))
    option2 = StrOption('unicode2', "Unicode 2", requires=[{'option': option1,
                                                            'expected': 'hide',
                                                            'action': 'hidden'}])
    descr1 = OptionDescription("options", "Common configuration", [option1, option2])
    descr = OptionDescription("choice1_requires", "Choice with requirement", [descr1])
    return descr
