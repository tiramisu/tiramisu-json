from tiramisu.option import UnicodeOption, OptionDescription
from tiramisu import Leadership

def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "Unicode leader", multi=True, properties=('hidden',))
    option1 = UnicodeOption('unicode1', "Unicode follower 1", multi=True)
    option2 = UnicodeOption('unicode2', "Unicode follower 2", multi=True)
    option3 = UnicodeOption('unicode3', "Unicode follower 3", multi=True)
    descr1 = Leadership("unicode", "Common configuration",
                          [option, option1, option2, option3])
    descr = OptionDescription("options", "Common configuration", [descr1])
    descr = OptionDescription("unicode1_leader_hidden_followers", "Leader follower with unicode and hidden leader", [descr])
    return descr
