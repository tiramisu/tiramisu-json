from tiramisu.option import UnicodeOption, OptionDescription
from tiramisu import Leadership


def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "Unicode leader", multi=True)
    option1 = UnicodeOption('unicode1', "Unicode follower 1", multi=True)
    option2 = UnicodeOption('unicode2', "Values 'test' must show 'Unicode follower 3'", multi=True)
    option3 = UnicodeOption('unicode3', "Unicode follower 3", requires=[{'option': option2,
                                                                      'expected': u'test',
                                                                      'action': 'hidden',
                                                                      'inverse': True}],
                            multi=True)
    descr1 = Leadership("unicode", "Common configuration 1",
                          [option, option1, option2, option3])
    descr = OptionDescription("options", "Common configuration 2", [descr1])
    descr = OptionDescription("unicode1_leadership_requires", "Leader followers with Unicode follower 3 hidden when Unicode follower 2 is test", [descr])
    return descr
