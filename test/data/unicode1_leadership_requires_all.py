from tiramisu.option import UnicodeOption, OptionDescription
from tiramisu import Leadership


def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "Value 'test' must show Leadership")
    option1 = UnicodeOption('unicode1', "Unicode leader", multi=True)
    option2 = UnicodeOption('unicode2', "Unicode follower 1", multi=True)
    option3 = UnicodeOption('unicode3', "Unicode follower 2", multi=True)
    descr1 = Leadership("unicode1", "Common configuration",
                          [option1, option2, option3], requires=[{'option': option,
                                                                  'expected': u'test',
                                                                  'action': 'hidden',
                                                                  'inverse': True}])
    descr = OptionDescription("options", "Common configuration", [option, descr1])
    descr = OptionDescription("unicode1_leadership_requires_all", "Leader follower with requirement", [descr])
    return descr
