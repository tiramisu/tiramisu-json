from tiramisu.option import UnicodeOption, OptionDescription
from tiramisu import Leadership


def get_description():
    """generate description for this test
    """
    option1 = UnicodeOption('unicode1', "Values 'test' must show 'Unicode follower 2'", multi=True)
    option2 = UnicodeOption('unicode2', "Unicode follower 1", multi=True)
    option3 = UnicodeOption('unicode3', "Unicode follower 2", multi=True,
                            requires=[{'option': option1,
                                       'expected': u'test',
                                       'action': 'hidden',
                                       'inverse': True}])
    descr1 = Leadership("unicode1", "Common configuration",
                          [option1, option2, option3])
    descr = OptionDescription("options", "Common configuration", [descr1])
    descr = OptionDescription("unicode1_leadership_requires_follower_value", "Leader follower requires follower with leader value", [descr])
    return descr


def get_values(api, allpath=False):
    if allpath:
        root = 'unicode1_leadership_requires_follower_value.'
    else:
        root = ''
    api.option(root + 'options.unicode1.unicode1').value.set([u'test', u'pas test'])
    api.option(root + 'options.unicode1.unicode2', 0).value.set(u'super1')
    api.option(root + 'options.unicode1.unicode3', 0).value.set(u'super1')
