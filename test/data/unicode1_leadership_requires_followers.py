from tiramisu.option import UnicodeOption, OptionDescription
from tiramisu import Leadership


def get_description():
    """generate description for this test
    """
    option1 = UnicodeOption('unicode1', "Unicode leader", multi=True)
    option2 = UnicodeOption('unicode2', "Values 'test' must show 'Unicode follower 2'", multi=True)
    option3 = UnicodeOption('unicode3', "Unicode follower 2", multi=True,
                            requires=[{'option': option2,
                                       'expected': u'test',
                                       'action': 'hidden',
                                       'inverse': True}])
    descr1 = Leadership("unicode1", "Common configuration",
                          [option1, option2, option3])
    descr = OptionDescription("options", "Common configuration", [descr1])
    descr = OptionDescription("unicode1_leadership_requires_followers", "Leader follower requires follower with a follower", [descr])
    return descr
