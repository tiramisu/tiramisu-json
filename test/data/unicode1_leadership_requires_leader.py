from tiramisu.option import UnicodeOption, OptionDescription
from tiramisu import Leadership


def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "Value 'test' must show leader")
    option1 = UnicodeOption('unicode1', "Unicode leader", multi=True, requires=[{'option': option,
                                                                                 'expected': u'test',
                                                                                 'action': 'hidden',
                                                                                 'inverse': True}])
    option2 = UnicodeOption('unicode2', "Unicode follower 1", multi=True)
    option3 = UnicodeOption('unicode3', "Unicode follower 2", multi=True)
    descr1 = Leadership("unicode1", "Common configuration",
                          [option1, option2, option3])
    descr = OptionDescription("options", "Common configuration", [option, descr1])
    descr = OptionDescription("unicode1_leadership_requires_leader", "Leader follower with requirement on leader", [descr])
    return descr
