"""just a multi unicode option
"""
from tiramisu.option import UnicodeOption, OptionDescription

def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "Multi string 1", multi=True)
    descr1 = OptionDescription("options", "Common configuration", [option])
    descr = OptionDescription("unicode1_multi", "Multi unicode", [descr1])
    return descr
