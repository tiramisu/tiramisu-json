"""just a multi unicode option
"""
from tiramisu.option import UnicodeOption, OptionDescription

def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "Unicode 1", multi=True, properties=('mandatory',))
    descr1 = OptionDescription("options", "Common configuration", [option])
    descr = OptionDescription("unicode1_multi_mandatory", "Mandatory multi Unicode", [descr1])
    return descr
