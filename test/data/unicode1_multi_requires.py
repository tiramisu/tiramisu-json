from tiramisu.option import StrOption, OptionDescription


def get_description():
    """generate description for this test
    """
    option1 = StrOption('unicode1', "Value 'test' must show Unicode 2")
    option2 = StrOption('unicode2', "Unicode 2", requires=[{'option': option1,
                                                            'expected': u'test',
                                                            'action': 'hidden',
                                                            'inverse': True}],
                        multi=True)
    descr1 = OptionDescription("options", "Common configuration", [option1, option2])
    descr = OptionDescription("unicode1_multi_requires", "Unicode multi with requirement", [descr1])
    return descr
