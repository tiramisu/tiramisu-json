"""just a multi unicode option
"""
from tiramisu.option import UnicodeOption, OptionDescription

def get_description():
    """generate description for this test
    """
    option = UnicodeOption('unicode', "String 1", ['a', 'b'], multi=True)
    descr1 = OptionDescription("options", "Common configuration", [option])
    descr = OptionDescription("unicode1_multi_value", "Multi unicode with default value 'a' and 'b'", [descr1])
    return descr

