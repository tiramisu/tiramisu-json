from tiramisu.option import UnicodeOption, OptionDescription


def get_description():
    """generate description for this test
    """
    option1 = UnicodeOption('unicode1', "Value 'test' must show OptionDescription")
    descr1 = OptionDescription("options", "Common configuration", [option1])
    option2 = UnicodeOption('unicode2', "Unicode 2")
    option3 = UnicodeOption('unicode3', "Unicode 3")
    descr2 = OptionDescription("unicode1", "OptionDescription with 2 options",
                               [option2, option3], requires=[{'option': option1,
                                                              'expected': u'test',
                                                              'action': 'hidden',
                                                              'inverse': True}])
    descr = OptionDescription("unicode1_optiondescription_requires", "OptionDesciption with requirement", [descr1, descr2])
    return descr
